#  CASA-based calibration of uGMRT data

_by Rohit Dokara; developed at MPIfR_

***
***

* &nbsp; Tested on Bands 3 & 4 of uGMRT continuum Stokes data, CASA 5.8.0, Linux
* &nbsp; Define parameters in `params.py` file; use field names, not IDs
* &nbsp; Run `params.py` and then `0-pipeInit.py` before starting the pipeline
* &nbsp; CASA MS should ALREADY EXIST..!
* &nbsp; Hanning smooth before starting this pipeline to get best continuum results
* &nbsp; If there are multiple bandpass/delay scans, best to use only one scan
* &nbsp; (ie flag unnecessary calibrator scans for better results)
* &nbsp; Log files (`./logsfolder/log.txt`, `./logsfolder/*.png`) will be overwritten
* &nbsp; Calibrators near the Galactic plane may need self-calibration!
* &nbsp; If averaging in frequency during split after calibration,
    resolution in MHz after averaging should be better than:
    1.5 (band-5), 0.7 (band-4), 0.4 (band-3), 0.1 (band-2)
* &nbsp; gain calibration uses `L1R` mode (CASA considers it experimental)
* &nbsp; Made for continuum, but can be modified for use in line data reduction as well
* &nbsp; Uses un-polarized calibrator to measure leakage during polarization calibration
* &nbsp; Polarization calibration is not well tested

***

* &nbsp; FLAGGING  &nbsp; &nbsp; [`calibration/1-initFlagging.py`]
    * flag bad antennas
    * flag shadowed antennas
    * flag bad scans
    * flag bad channels
    * quack beginning of each scan
    * clip outside given ranges
    * mild tfcrop on all data
    * plot data
***

* &nbsp; DELAY/BP/polcal SOLUTIONS  &nbsp; &nbsp; [`calibration/2-getDBP.py`]
    * split delay/BP[/pol] calibrators to a new MS
    * set calibrator models for delay/BP[/pol] calibrators
    * [1] choose refant, get delay+BP (using phase averaged data, solint = init_solint, smallspw)
    * [2] if doing polarization calibration:
        * ap-gain calibration (solint = init_solint, full spw)
        * split polarized and un-polarized calibrators to a new MS
        * get cross-hand delays (solint = 'inf', full spw)
        * get leakage terms (polcal, 'Df', solint = leak_solint, full spw)
        * fix polarization angle (polcal, 'Xf', solint = 'inf', full spw)
    * flag BP/delay[/pol] fields using applycal
    * repeat steps [1] and [2]
    * apply solutions and flag BP/delay[/pol] fields using tfcrop
    * [FINAL] repeat steps [1] and [2]
***

* &nbsp; GAIN SOLUTIONS  &nbsp; &nbsp; [`calibration/3-getGains.py`]
    * [FINAL] p gaincal w/ BP calibrator, solint = 'inf', gaincalspw
    * unflag any flagged solutions in the above table
    * apply all previous tables on all calibrators
    * split all calibrator data (corrected) to a new MS with channel averaging
    * set model of phase calibrator if it is available
    * p gaincal w/ phase calibrator, solint = init_solint, gaincalspw
    * ap gaincal w/ phase calibrator, solint = main_solint, gaincalspw
    * apply solutions on phase calibrator (interp='nearest')
    * flag corrected data of phase calibrator with tfcrop
    * p gaincal w/ flux and phase calibrators, solint = init_solint, gaincalspw
    * ap gaincal w/ flux and phase calibrators, solint = main_solint, gaincalspw
    * [FINAL] transfer flux scaling from flux to phase calibrator
    * [FINAL] p gaincal w/ phase calibrator, solint = main_solint, gaincalspw
    * apply scaled_fx and short_p with 'nearest' interp and make plots (for diagnostics)
***

* &nbsp; CALIBRATING ALL THE FIELDS  &nbsp; &nbsp; [`calibration/4-applyTables.py`]
    * apply calibration solutions
    * tfcrop on all baselines at once (on corrected data column)
    * rflag on long/short baselines separately (on corrected data column)
    * plot corrected data (amp/phase vs UV) of all fields
    * split target with defined averaging and then statwt
***

* &nbsp; PRELIMINARY IMAGES OF CALIBRATOR  &nbsp; &nbsp; [`calibration/checkCals.py`]
    * Test-image primary and secondary calibrators (parameters to be set manually)
***



* &nbsp; "Inspired" by VLA scripted and online uGMRT pipelines, also CASA tutorials
* &nbsp; Much faster than other uGMRT pipelines because of split+averaging for gains
* &nbsp; Diagnostic plots are output for almost all the steps :)
* &nbsp; CASA VLA pipeline calibration strategy is used, except for a few tasks that were automated in that pipeline such as solint choosing.  These have to be set manually in this pipeline.


