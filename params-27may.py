#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!

logsfolder   = './logsfolder/'          # folder to output log and plots
pipe_log     = './logsfolder/log.txt'   # log file
myms         = '27may.HS.ms'            # MS to use
freqs        = 4096                     # number of channels
fluxfield    = '3C286'                  # calibrators and targets
phasefield   = '1851+005'
bpassfield   = '3C286'
delayfield   = '3C286'
targfields   = '4SRC001,4SRC002,4SRC003,4SRC101,4SRC102,4SRC105,4SRC201,4SRC202,4SRC204,4SRC205,4SRC301,4SRC302,4SRC303'
refantenna   = 'C02'                    # initial reference antenna: central, good

# initial flagging
flagants     = 'C03,S02,S03,S04,S06,W06'# bad antenna names (C02 etc, '' if none)
flagscans    = ''                       # bad scan numbers ('' if none)
flagspwRFI   = ''                       # flag bad channels/windows (give it in channel numbers only)
edgechans    = 0.02                     # fraction of edge channels to flag
clipTF       = False                    # if False, below clip params are not used
clipflux     = [0.0, 200.0]             # flag outside this range, flux calibrator
clipbpass    = [0.0, 200.0]             # "  bandpass calibrator
clipdelay    = [0.0, 200.0]             # "  delay calibrator
clipphase    = [0.0, 100.0]             # "  phase calibrator
quackTF      = False                    # do quack flagging? (beginning)
quackint     = 11.0                     # quack the beginning time in seconds (in general 1 or 2 integrations)

# calibration
uvracal      = '>1klambda'              # use only this uv-range for calibration
smallspw     = '0:660MHz~665MHz'        # RFI free channels for initial gaincal (give it in MHz; leave as '' if unsure of RFI situation).  # '0:5734~6554'
gaincalspw   = '0:560MHz~720MHz'        # high gain channels for gaincal (give it in MHz; leave as '' if unsure of RFI situation).  # '0:3278~15155'
init_solint  = '11s'                    # for initial phase calibration (<1 min)
main_solint  = 'inf'                    # for main gain calibration (ap-)
phase_model  = ''                       # model image of phase calibrator
phase_model  = '/home/uGMRT/1851+005_22aug_10k_b4.model.tt0'
channelavg_c = 10                       # channel averaging for calibrator gains

# polarization calibration (if False, all params in this section are not used)
polcalibTF   = False                    # do polarization calibration?
unpolcalib   = '3C48'                   # unpolarized calibrator field (for D terms)
unpolcal_knw = True                     # is it a known calibrator field?
unpolcal_FD  = ''                       # flux density of the unpolarized calibrator. Not used if it is a known source
polcalib     = '3C286'                  # polarized calibrator field (for X-delays and PA fixing)
polcalib_FD  = [17.705166,0,0,0]        # its flux density (CASA can calculate Q,U from details below)
polcalib_SI  = [-0.4507,-0.1798,0.0357] # its spectral index (follow CASA convention!)
polcalib_pi  = [0.03]                   # its polarization fraction expression coefficients (see setjy. ideally: [0.0846,0.0306,-0.018]. but at these frequencies 3C286 is only 3% polarized)
polcalib_PA  = [0.57595865,0.]          # its polarization position angle in radian
polcalib_RM  = 0.                       # its rotation measure
polcalib_fq  = '1GHz'                   # reference frequency for the above values
pol_solint   = 'inf,4MHz'               # solint parameter for polcal (Df,Xf)

# targets
flag_targ    = True                     # flag the target fields after applycal?
targ_split   = True                     # split the targets?
targ_statwt  = True                     # perform statwt after splitting the target?
channelavg_t = 12                       # number of channels to average after splitting targets


###
# --- do the actual data reduction ---
###

# importing, Hanning smoothing, listobs, params
importgmrt("/diskk/uGMRT/raw-data/36_061_27MAY2019_GWB.GWB1.LTA_RRLL.RRLLFITS.fits", vis="27may.ms")
hanningsmooth("27may.ms", outputvis="temp.ms")
split("temp.ms",outputvis="27may.HS.ms",width=2,datacolumn='data')
listobs("27may.HS.ms", verbose=True, listfile="27may.HS.listobs")
# now define the above parameters in this file (copy and paste)

execfile('/home/uGMRT/5.8.0.v4/0-pipeInit.py')                         # define required functions
execfile('/home/uGMRT/5.8.0.v4/calibration/1-initFlagging.py')         # initial flagging before calibration
execfile('/home/uGMRT/5.8.0.v4/calibration/2-getDBP.py')               # get delay and bandpass calibration tables
execfile('/home/uGMRT/5.8.0.v4/calibration/3-getGains.py')             # get gain calibration tables
execfile('/home/uGMRT/5.8.0.v4/calibration/4-applyTables.py')          # calibrate the targets and flag residual RFI




























