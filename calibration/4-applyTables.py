#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!

writelog("\n")
writelog("PREPARING FINAL TARGET DATA ...")
writelog("")

if polcalibTF==True:
    gaintabs_list = [delays,bpass,avg_p,pol_KCR,pol_leak,pol_PA,long_p,scaled_fx]
    gaintabs_intr = ['',',linearflag','',    '',    '',     '',   '',   '']
else:
    gaintabs_list = [delays,bpass,avg_p,long_p,scaled_fx]
    gaintabs_intr = ['',',linearflag','','',   '']

writelog("Calibrating all the target fields ...")
applycal(vis = myms,
         field = targfields,
         applymode = 'calflagstrict',
         gaintable = gaintabs_list,
         interp    = gaintabs_intr,
         calwt = False,
         flagbackup = False,
         parang = True)
flagmanager(vis = myms,
            mode = 'save',
            versionname = 'appliedfinaltabs',
            comment = 'after final applycal on targets',
            merge = 'replace')
flagsummarywrite(myms, fields=allfields)

if flag_targ==True:
    writelog("Flagging on targets ...")
    writelog("  tfcrop ...")
    flagdata(vis = myms,
             mode = 'tfcrop',
             field = targfields,
             datacolumn = 'corrected',
             timefit = 'line',
             freqfit = 'poly',
             timecutoff = 4.5,
             freqcutoff = 3.5,
             #usewindowstats = 'both',
             #halfwin = 2,
             extendflags = True,
             flagbackup = False)
    writelog("  rflag ...")
    flagdata(vis = myms,
             mode = 'rflag',
             field = targfields,
             datacolumn = 'corrected',
             uvrange = '>1klambda',
             timedevscale = 5.,
             freqdevscale = 5.,
             extendflags = True,
             flagbackup = False)
    flagdata(vis = myms,
             mode = 'rflag',
             field = targfields,
             datacolumn = 'corrected',
             uvrange = '<1klambda',
             timedevscale = 5.,
             freqdevscale = 5.,
             extendflags = False,
             flagbackup = False)
    flagmanager(vis = myms,
                mode = 'save',
                versionname = 'lastflags',
                comment = 'last flags',
                merge = 'replace')
    flagsummarywrite(myms, fields=targfields)
    writelog("")

# final plots
writelog("Making plots of targets...")
for fld in targfields.split(','):
    for yax in ['amp','phase']:
        plotfilepath = logsfolder + '04-0-'+yax+'_UV-'+fld+'.png'
        writelog("Plotting " + plotfilepath + " ...")
        plotms( vis = myms,
                xaxis = 'UVwave',
                yaxis = yax,
                ydatacolumn = 'corrected',
                coloraxis = 'antenna1',
                field = fld,
                correlation = 'RR,LL',
                averagedata = True,
                avgtime = '1e8',
                avgchannel = '1e8',
                scalar = True,
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all' )
for yax in ['amp','phase']:
    plotfilepath = logsfolder + '04-1-'+yax+'_time.png'
    writelog("Plotting " + plotfilepath + " ...")
    plotms( vis = myms,
            xaxis = 'time',
            yaxis = yax,
            xselfscale = True,
            yselfscale = True,
            ydatacolumn = 'corrected',
            field = targfields,
            coloraxis = 'field',
            iteraxis = 'baseline',
            correlation = 'RR,LL',
            averagedata = True,
            avgspw = True,
            avgchannel = '1e8',
            scalar = True,
            antenna = refantenna,
            width = 1500,
            height = 900,
            gridrows = 4,
            gridcols = 3,
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')

if targ_split==True:
    writelog("Splitting targets ...")
    subprocess.Popen(['mkdir', 'targets'])
    time.sleep(3)       # giving enough time to create the folder
    for targID in targfields.split(','):
        subprocess.Popen(['mkdir', 'targets/'+targID])
        time.sleep(3)       # giving enough time to create the folder
        split(vis = myms,
              outputvis = 'targets/'+targID+'/'+myms[:-3]+'_'+targID+'.ms',
              datacolumn = 'corrected',
              field = targID,
              width = channelavg_t,
              keepflags = False)
        if targ_statwt==True:
            statwt( 'targets/'+targID+'/'+myms[:-3]+'_'+targID+'.ms',
                    datacolumn='data', timebin='30s', chanbin=5 )

writelog("")
writelog("----------------------------------------")
writelog("            calibration done            ")
writelog("----------------------------------------")







