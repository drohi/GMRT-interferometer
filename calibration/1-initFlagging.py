#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!

writelog("\n")
writelog("INITIAL FLAGGING")
writelog("")

# flag bad antennas
if flagants != '':
    writelog("Flagging bad antennas ...")
    flagdata(vis = myms,
             mode = 'manual',
             field = allfields,
             antenna = flagants,
             flagbackup = False)
else:
    writelog("No bad antennas were given.")

# flag shadowed antennas
writelog("Flagging shadowed antennas ...")
flagdata(vis = myms,
         mode = 'shadow',
         field = allfields,
         antenna = flagants,
         flagbackup = False)

# flag bad scans
if flagscans != '':
    writelog("Flagging bad scans ...")
    flagdata(vis = myms,
             mode = 'manual',
             scan = flagscans,
             flagbackup = False)
else:
    writelog("No bad scans were given.")

# flag bad channels (RFI and end)
writelog("Flagging bad channels ...")
flagchans = '0:0'
if edgechans != 0:
    for k in range(1,int(round(edgechans*freqs))):                      # left edge channels
        flagchans += ';' + str(k)
    for k in range(int(round( (1-edgechans)*freqs )), freqs):           # right edge channels
        flagchans += ';' + str(k)
if flagspwRFI != '':                                                    # if RFI list is given
    flagchans = flagchans+','+flagspwRFI
if freqs == 1:
    writelog("Parameter freqs was set to 1, so skipping channel-flagging.")
elif edgechans == 0 and flagspwRFI == '':
    writelog("Parameter edgechans was set to 0, and no RFI was mentioned, so skipping channel-flagging.")
else:
    flagdata(vis = myms,
             mode = 'manual',
             field = allfields,
             spw = flagchans,
             flagbackup = False)

# quack-flagging
if quackTF==True:
    writelog("Quack flagging ...")
    flagdata(vis = myms,
             mode = 'quack',
             field = allfields,
             quackinterval = quackint,
             quackmode = 'beg',
             flagbackup = False)
else:
    writelog("Skipping quack-flagging ...")

# clip flagging for various fields
if clipTF==True:
    writelog("Clip flagging ...")
    flagdata(vis = myms,
             mode = "clip",
             field = fluxfield,
             clipminmax = clipflux,
             clipzeros = True,
             extendpols = False,
             flagbackup = False)
    if fluxfield != phasefield:
        flagdata(vis = myms,
                 mode = "clip",
                 field = phasefield,
                 clipminmax = clipphase,
                 clipzeros = True,
                 extendpols = False,
                 flagbackup = False)
    if fluxfield != bpassfield and phasefield != bpassfield:
        flagdata(vis = myms,
                 mode = "clip",
                 field = bpassfield,
                 clipminmax = clipbpass,
                 clipzeros = True,
                 extendpols = False,
                 flagbackup = False)
    if bpassfield != delayfield and fluxfield != delayfield and phasefield != delayfield:
        flagdata(vis = myms,
                 mode = "clip",
                 field = delayfield,
                 clipminmax = clipdelay,
                 clipzeros = True,
                 extendpols = False,
                 flagbackup = False)
else:
    writelog("Skipping clip-flagging ...")

# mild tfcrop on all cals
writelog("tfcrop flagging ...")
flagdata(vis = myms,
         mode = 'tfcrop',
         #field = allfields,
         field = allcals,
         datacolumn = 'data',
         timefit = 'poly',
         freqfit = 'poly',
         timecutoff = 5.,
         freqcutoff = 5.,
         extendflags = True,
         flagbackup = False)

# save flags
flagmanager(vis = myms,
            mode = 'save',
            versionname = 'initflags',
            comment = 'All necessary initial flags',
            merge = 'replace')

# flagging summary
flagsummarywrite(myms, fields=allfields)
#flagsummarywrite(myms, fields=allcals)

# some initial plots
plotfilepath = logsfolder + '01-elevation_time.png'
writelog("Plotting " + plotfilepath + " ...")
plotms(vis = myms,
       xaxis = 'time',
       yaxis = 'elevation',
       antenna = refantenna,
       coloraxis = 'field',
       plotfile = plotfilepath,
       overwrite = True,
       showgui = False)
plotfilepath = logsfolder + '01-ants_linear.png'
writelog("Plotting " + plotfilepath + " ...")
plotants(vis = myms,
         antindex = True,
         logpos = False,
         figfile = plotfilepath,
         showgui = False)
plotfilepath = logsfolder + '01-ants_log.png'
writelog("Plotting " + plotfilepath + " ...")
plotants(vis = myms,
         antindex = True,
         logpos = True,
         figfile = plotfilepath,
         showgui = False)
plotfilepath = logsfolder + '01-amp_freq-cals.png'
writelog("Plotting " + plotfilepath + " ...")
plotms(vis = myms,
       xaxis = 'freq',
       yaxis = 'amp',
       xselfscale = True,
       yselfscale = True,
       field = allcals,
       iteraxis = 'baseline',
       coloraxis = 'field',
       correlation = 'RR',
       averagedata = True,
       avgtime = '1e8',
       scalar = True,
       antenna = refantenna,
       width = 1500,
       height = 900,
       gridrows = 3,
       gridcols = 2,
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')
plotfilepath = logsfolder + '01-amp_time.png'
writelog("Plotting " + plotfilepath + " ...")
plotms(vis = myms,
       xaxis = 'time',
       yaxis = 'amp',
       xselfscale = True,
       yselfscale = True,
       field = allfields,
       iteraxis = 'baseline',
       coloraxis = 'field',
       correlation = 'RR',
       averagedata = True,
       avgchannel = '1e8',
       scalar = True,
       antenna = refantenna,
       width = 1500,
       height = 900,
       gridrows = 3,
       gridcols = 2,
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')

writelog("")
writelog("-- Initial flagging done --")
