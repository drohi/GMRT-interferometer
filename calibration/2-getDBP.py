#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!
# polcal may require fixed refant. Maybe use refantenna.split(',')[0] for that?

writelog("\n")
writelog("DELAY+BP TABLES")
writelog("")

writelog("Splitting DBP calibrators data ...")
os.system('rm -rf '+DBPcalsMS)
os.system('rm -rf '+DBPcalsMS+'.flagversions')
split(vis = myms,
      field = DBPcals,
      datacolumn = 'data',
      outputvis = DBPcalsMS,
      keepflags = False)

# setjy on BP/delay fields
writelog("Running setjy ...")
setjy(vis=DBPcalsMS, field=fluxfield, standard='Perley-Butler 2017')
if delayfield != fluxfield:
    setjy(vis=DBPcalsMS, field=delayfield, standard='Perley-Butler 2017')
if bpassfield != delayfield and bpassfield != fluxfield:
    setjy(vis=DBPcalsMS, field=bpassfield, standard='Perley-Butler 2017')
if polcalibTF==True:
    setjy( vis=DBPcalsMS, field=polcalib, standard='manual',
           reffreq=polcalib_fq, fluxdensity=polcalib_FD, spix=polcalib_SI,
           polindex=polcalib_pi, polangle=polcalib_PA, rotmeas=polcalib_RM )
    if unpolcal_knw==True:
        setjy( vis=DBPcalsMS, field=unpolcalib )

# define a function so that we can repeat this action multiple times
def getDelayBP(idx):
    # delay calibration
    writelog("  p-gaincal w/ delay calibrator ...")
    rmtables(p_delay)
    gaincal(vis = DBPcalsMS,
            caltable = p_delay,
            field = delayfield,
            gaintype = 'G',
            calmode = 'p',
            spw = smallspw,
            minsnr = 2.5,
            solint = init_solint,
            solmode = 'L1R',
            uvrange = uvracal,
            refant = refantenna,
            parang = True)
    flaggedSolnResult = getCalFlaggedSoln(p_delay)
    writelog("    Fraction of flagged solutions = " + \
             str(flaggedSolnResult['all']['fraction']) )
    writelog("    Median fraction of flagged solutions per antenna = " + \
             str(flaggedSolnResult['antmedian']['fraction']) )
    plotfilepath = logsfolder + '02-r' + str(idx) + '-0-p_delay-phas_time.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = p_delay,
            xaxis = 'time',
            yaxis = 'phase',
            xselfscale = True,
            gridrows = 4,
            gridcols = 3,
            width = 1500,
            height = 900,
            iteraxis = 'antenna',
            coloraxis = 'corr',
            plotrange = [-1,-1,-180,180],
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')
    writelog("  delay calibration ...")
    rmtables(delays)
    gaincal(vis = DBPcalsMS,
            caltable = delays,
            gaintype = 'K',
            solint = 'inf',
            solmode = 'L1R',
            combine = 'scan',
            field = delayfield,
            gaintable = [p_delay],
            refant = refantenna,
            parang = True)
    flaggedSolnResult = getCalFlaggedSoln(delays)
    writelog("    Fraction of flagged solutions = " + \
             str(flaggedSolnResult['all']['fraction']) )
    writelog("    Median fraction of flagged solutions per antenna = " + \
             str(flaggedSolnResult['antmedian']['fraction']) )
    plotfilepath = logsfolder + '02-r' + str(idx) + '-1-delays.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = delays,
            xaxis = 'antenna1',
            yaxis = 'delay',
            coloraxis = 'baseline',
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath)
    # bandpass calibration
    writelog("  ap-gaincal w/ BP calibrator ...")
    rmtables(ap_bpass)
    gaincal(vis = DBPcalsMS,
            caltable = ap_bpass,
            field = bpassfield,
            gaintype = 'G',
            calmode = 'ap',
            spw = smallspw,
            minsnr = 2.0,
            solint = init_solint,
            solmode = 'L1R',
            gaintable = [delays],
            uvrange = uvracal,
            refant = refantenna,
            parang = True)
    flaggedSolnResult = getCalFlaggedSoln(ap_bpass)
    writelog("    Fraction of flagged solutions = " + \
             str(flaggedSolnResult['all']['fraction']) )
    writelog("    Median fraction of flagged solutions per antenna = " + \
             str(flaggedSolnResult['antmedian']['fraction']) )
    plotfilepath = logsfolder + '02-r' + str(idx) + '-2-ap_bpass-phas_time.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = ap_bpass,
            xaxis = 'time',
            yaxis = 'phase',
            xselfscale = True,
            gridrows = 4,
            gridcols = 3,
            width = 1500,
            height = 900,
            iteraxis = 'antenna',
            coloraxis = 'corr',
            plotrange = [-1,-1,-180,180],
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')
    plotfilepath = logsfolder + '02-r' + str(idx) + '-2-ap_bpass-amp_time.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = ap_bpass,
            xaxis = 'time',
            yaxis = 'amp',
            xselfscale = True,
            yselfscale = True,
            gridrows = 4,
            gridcols = 3,
            width = 1500,
            height = 900,
            iteraxis = 'antenna',
            coloraxis = 'corr',
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')
    writelog("  bandpass calibration ...")
    rmtables(bpass)
    bandpass(   vis = DBPcalsMS,
                caltable = bpass,
                solint = 'inf',
                minsnr = 4.0,
                solnorm = True,
                field = bpassfield,
                refant = refantenna,
                gaintable = [delays, ap_bpass],
                uvrange = uvracal,
                parang = True)
    flaggedSolnResult = getCalFlaggedSoln(bpass)
    writelog("    Fraction of flagged solutions = " + \
             str(flaggedSolnResult['all']['fraction']) )
    writelog("    Median fraction of flagged solutions per antenna = " + \
             str(flaggedSolnResult['antmedian']['fraction']) )
    plotfilepath = logsfolder + '02-r' + str(idx) + '-3-bpass_amp-freq.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = bpass,
            xaxis = 'freq',
            yaxis = 'amp',
            xselfscale = True,
            yselfscale = True,
            gridrows = 4,
            gridcols = 3,
            width = 1500,
            height = 900,
            iteraxis = 'antenna',
            coloraxis = 'corr',
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')
    plotfilepath = logsfolder + '02-r' + str(idx) + '-3-bpass_phase-freq.png'
    writelog("    Plotting " + plotfilepath + " ...")
    plotms( vis = bpass,
            xaxis = 'freq',
            yaxis = 'phase',
            xselfscale = True,
            gridrows = 4,
            gridcols = 3,
            width = 1500,
            height = 900,
            iteraxis = 'antenna',
            coloraxis = 'corr',
            plotrange = [-1,-1,-180,180],
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')
    # do the next only for polarization calibration
    if polcalibTF==True:
        writelog("  ap-gaincal w/ pol & unpol calibrators ...")
        rmtables(pol_ap)
        gaincal(vis = DBPcalsMS,
                caltable = pol_ap,
                field = polcalib +','+ unpolcalib,
                gaintype = 'G',
                calmode = 'ap',
                minsnr = 2.5,
                solint = init_solint,
                solmode = 'L1R',
                gaintable = [delays,bpass],
                uvrange = uvracal,
                refant = refantenna,
                parang = True)
        flaggedSolnResult = getCalFlaggedSoln(pol_ap)
        writelog("    Fraction of flagged solutions = " + \
                str(flaggedSolnResult['all']['fraction']) )
        writelog("    Median fraction of flagged solutions per antenna = " + \
                str(flaggedSolnResult['antmedian']['fraction']) )
        plotfilepath = logsfolder + '02-r' + str(idx) + '-4-pol_ap-phas_time.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_ap,
                xaxis = 'time',
                yaxis = 'phase',
                xselfscale = True,
                gridrows = 4,
                gridcols = 3,
                width = 1500,
                height = 900,
                iteraxis = 'antenna',
                coloraxis = 'corr',
                plotrange = [-1,-1,-180,180],
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all')
        plotfilepath = logsfolder + '02-r' + str(idx) + '-4-pol_ap-amp_time.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_ap,
                xaxis = 'time',
                yaxis = 'amp',
                xselfscale = True,
                yselfscale = True,
                gridrows = 4,
                gridcols = 3,
                width = 1500,
                height = 900,
                iteraxis = 'antenna',
                coloraxis = 'corr',
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all')
        # perform scaling only if un-polarized calibrator is a source unknown to CASA
        if unpolcal_knw!=True:
            writelog("  un-polarized calibrator is a source unknown to CASA")
            writelog("  scaling the gains ...")
            rmtables(pol_fx)
            calfluxes = fluxscale(vis = DBPcalsMS,
                                caltable = pol_ap,
                                fluxtable = pol_fx,
                                reference = polcalib,
                                transfer = unpolcalib,
                                incremental = False)
            writelog("    Flux density (Jy): " +
                    str(int(calfluxes['1']['0']['fluxd'][0]*1e5)/1e5) + '+/-' +
                    str(int(calfluxes['1']['0']['fluxdErr'][0]*1e5)/1e5) )
            writelog("    Spectral index: " +
                    str(int(calfluxes['1']['spidx'][0])) + '+/-' +
                    str(int(calfluxes['1']['spidxerr'][0])) )
            writelog("    Frequency (MHz): "+ str(int(calfluxes['1']['fitRefFreq']/1e6)))
            writelog("    Number of solutions: "+ str(int(calfluxes['1']['0']['numSol'][0])))
            writelog("  applying delay, BP, gain tables ...")
        else:
            pol_fx = pol_ap         # use the correct gain table
            writelog("  un-polarized calibrator is a source known to CASA")
            writelog("  no need to scale the gains ...")
        applycal(vis = DBPcalsMS,
                 applymode = 'calflagstrict',
                 gaintable = [delays,bpass,pol_fx],
                 interp    = ['',',linearflag',''],
                 calwt = False,
                 flagbackup = False,
                 parang = False)
        writelog("  splitting to a new MS ...")
        os.system('rm -rf '+polcalsMS)
        os.system('rm -rf '+polcalsMS+'.flagversions')
        split(vis = DBPcalsMS,
              field = polcalib+','+unpolcalib,
              datacolumn = 'corrected',
              outputvis = polcalsMS,
              keepflags = False)
        writelog("  setting models on the new MS ...")
        setjy( vis=polcalsMS, field=polcalib, standard='manual',
               reffreq=polcalib_fq, fluxdensity=polcalib_FD, spix=polcalib_SI,
               polindex=polcalib_pi, polangle=polcalib_PA, rotmeas=polcalib_RM )
        if unpolcal_knw!=True:              # if un-polarized calibrator is a source unknown to CASA
            setjy( vis=polcalsMS, field=unpolcalib, standard='manual',
                   fluxdensity=calfluxes['1']['fitFluxd'] )
        else:
            setjy( vis=polcalsMS, field=unpolcalib )
        writelog("  getting cross-hand delays ...")
        gaincal(vis = polcalsMS,
                caltable = pol_KCR,
                field = polcalib,
                gaintype = 'KCROSS',
                solint = 'inf',
                solmode = 'L1R',
                refant = refantenna,
                parang = True)
        flaggedSolnResult = getCalFlaggedSoln(pol_KCR)
        writelog("    Fraction of flagged solutions = " + \
                str(flaggedSolnResult['all']['fraction']) )
        writelog("    Median fraction of flagged solutions per antenna = " + \
                str(flaggedSolnResult['antmedian']['fraction']) )
        plotfilepath = logsfolder + '02-r' + str(idx) + '-5-pol_KCR.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_KCR,
                xaxis = 'antenna1',
                yaxis = 'delay',
                #antenna = refantenna,
                coloraxis = 'corr',
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath)
        writelog("  solving for leakage terms ...")
        polcal( vis = polcalsMS,
                caltable = pol_leak,
                field = unpolcalib,
                refant = refantenna.split(',')[0],
                solint = pol_solint,
                poltype = 'Df',
                gaintable = pol_KCR )
        flaggedSolnResult = getCalFlaggedSoln(pol_leak)
        writelog("    Fraction of flagged solutions = " + \
                str(flaggedSolnResult['all']['fraction']) )
        writelog("    Median fraction of flagged solutions per antenna = " + \
                str(flaggedSolnResult['antmedian']['fraction']) )
        plotfilepath = logsfolder + '02-r' + str(idx) + '-6-leak_amp-freq.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_leak,
                xaxis = 'freq',
                yaxis = 'amp',
                xselfscale = True,
                yselfscale = True,
                gridrows = 4,
                gridcols = 3,
                width = 1500,
                height = 900,
                iteraxis = 'antenna',
                coloraxis = 'corr',
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all')
        plotfilepath = logsfolder + '02-r' + str(idx) + '-6-leak_phase-freq.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_leak,
                xaxis = 'freq',
                yaxis = 'phase',
                xselfscale = True,
                gridrows = 4,
                gridcols = 3,
                width = 1500,
                height = 900,
                iteraxis = 'antenna',
                coloraxis = 'corr',
                plotrange = [-1,-1,-180,180],
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all')
        writelog("  solving for position angle ...")
        polcal( vis = polcalsMS,
                caltable = pol_PA,
                field = polcalib,
                refant = refantenna,
                solint = pol_solint,
                poltype = 'Xf',
                gaintable = [pol_KCR,pol_leak] )
        flaggedSolnResult = getCalFlaggedSoln(pol_PA)
        writelog("    Fraction of flagged solutions = " + \
                str(flaggedSolnResult['all']['fraction']) )
        writelog("    Median fraction of flagged solutions per antenna = " + \
                str(flaggedSolnResult['antmedian']['fraction']) )
        plotfilepath = logsfolder + '02-r' + str(idx) + '-7-polPA_phase-freq.png'
        writelog("    Plotting " + plotfilepath + " ...")
        plotms( vis = pol_PA,
                xaxis = 'freq',
                yaxis = 'phase',
                antenna = refantenna.split(',')[0],
                coloraxis = 'corr',
                plotrange = [-1,-1,-180,180],
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath )

# decide which caltables to use based on polarization calibration
if polcalibTF==True:
    # fix the correct gain table
    if unpolcal_knw!=True:      # if un-pol calib an unknown source
        pass                    # use pol_fx as it is
    else:
        pol_fx = pol_ap         # use pol_ap
    gaintabs_list = [delays,bpass,pol_fx,pol_KCR,pol_leak,pol_PA]
    gaintabs_intr = ['',',linearflag','',     '',    '',  '']
else:
    gaintabs_list = [delays,bpass]
    gaintabs_intr = ['',',linearflag']

# set reference antenna using heuristics if not being forced by user
if forceantenna==False:
    findrefant = RefAntHeuristics(vis=DBPcalsMS, geometry=True, flagging=True)
    RefAntOut  = findrefant.calculate()
    refantenna = str(RefAntOut[0]) + ',' + str(RefAntOut[1]) + ',' + \
                 str(RefAntOut[2]) + ',' + str(RefAntOut[3])
    refantenna = refantenna.upper()
writelog("Reference antenna(s) being used: " + refantenna)

# get initial solutions, apply, and flag corrected data
writelog("DBP round: 0")
getDelayBP(0)
writelog("Running applycal (flag only) ...")
applycal(vis = DBPcalsMS,
         applymode = 'flagonlystrict',
         gaintable = gaintabs_list,
         interp    = gaintabs_intr,
         calwt = False,
         flagbackup = False,
         parang = True)
# get solutions again, apply, flag corrected data
writelog("DBP round: 1")
getDelayBP(1)
writelog("Running applycal ...")
applycal(vis = DBPcalsMS,
         applymode = 'calflagstrict',
         gaintable = gaintabs_list,
         interp    = gaintabs_intr,
         calwt = False,
         flagbackup = False,
         parang = True)
writelog("Flagging ...")
flagdata(vis = DBPcalsMS,
         mode = 'tfcrop',
         datacolumn = 'corrected',
         timefit = 'poly',
         freqfit = 'line',
         timecutoff = 4.,
         freqcutoff = 4.,
         maxnpieces = 5,
         extendflags = False,
         flagbackup = False)
# get final solutions
writelog("DBP round: 2")
getDelayBP(2)

# save flags and write summary
flagmanager(vis = DBPcalsMS,
            mode = 'save',
            versionname = 'afterDBP',
            comment = 'end of 2-getDBP.py',
            merge = 'replace')
flagsummarywrite(DBPcalsMS)

writelog("")
if polcalibTF==True:
    writelog("-- polcal, delay and bandpass tables are ready --")
else:
    writelog("-- Delay and bandpass tables are ready --")



