#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!

writelog("\n")
writelog("GAIN TABLES")
writelog("")

# which caltables to use
if polcalibTF==True:
    gaintabs_list = [delays,bpass,pol_KCR,pol_leak,pol_PA]
    gaintabs_intr = ['',',linearflag','',    '',    '']
    apply_parang  = False
else:
    gaintabs_list = [delays,bpass]
    gaintabs_intr = ['',',linearflag']
    apply_parang  = True

# p-gaincal ('inf' solint) w/ BP calibrator (average phase gain)
# EVLA pipeline also does this just to make plots easier to interpret
writelog("inf solint p-gaincal with BP calibrator ...")
rmtables(avg_p)
gaincal(vis = myms,
        caltable = avg_p,
        field = bpassfield,
        gaintype = 'G',
        calmode = 'p',
        spw = gaincalspw,
        minsnr = 1.0,
        gaintable = gaintabs_list,
        interp = gaintabs_intr,
        solint = 'inf',
        solmode = 'L1R',
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
# unflag the above table (let the bad antennas be flagged in other tables)
flagdata(avg_p,mode='unflag',flagbackup=False)

# correct list of caltables to use
gaintabs_list = gaintabs_list + [avg_p]
gaintabs_intr = gaintabs_intr + ['']
writelog("Calibration tables until now: ")
for ii in range(len(gaintabs_list)):
    writelog("  "+str(gaintabs_list[ii]))

# apply DBP tables to flag bad data
writelog("Applying current tables to all calibrators ...")
applycal(vis = myms,
         field = allcals,
         applymode = 'calflagstrict',
         gaintable = gaintabs_list,
         interp    = gaintabs_intr,
         calwt = False,
         flagbackup = False,
         parang = apply_parang)
flagmanager(vis = myms,
            mode = 'save',
            versionname = 'appliedDBPallcals',
            comment = 'after 1st applycal in 3-getGains.py',
            merge = 'replace')

# split all calibrators to a new MS to make processing faster
writelog("Splitting calibrator data ...")
os.system('rm -rf '+calsMS)
os.system('rm -rf '+calsMS+'.flagversions')
split(vis = myms,
      field = allcals,
      width = channelavg_c,
      datacolumn = 'corrected',
      outputvis = calsMS,
      keepflags = False)
flagmanager(vis = calsMS,
            mode = 'save',
            versionname = 'initial',
            comment = 'before anything else',
            merge = 'replace')

# setjy on phase calibrator field
if phase_model == '':
    writelog("No phase calibrator model provided.")
    writelog("Using point source approximation.")
else:
    writelog("Using user-defined model for phase calibrator.")
    setjy(vis=calsMS, field=phasefield, model=phase_model)

# setjy on BP/delay fields
writelog("Running setjy ...")
setjy(vis=calsMS, field=fluxfield)
if delayfield != fluxfield:
    setjy(vis=calsMS, field=delayfield)
if bpassfield != delayfield and bpassfield != fluxfield:
    setjy(vis=calsMS, field=bpassfield)

# set reference antenna using heuristics if not being forced by user
if forceantenna==False:
    findrefant = RefAntHeuristics(vis=calsMS, field=phasefield, \
                                  geometry=True, flagging=True)
    RefAntOut  = findrefant.calculate()
    refantenna = str(RefAntOut[0]) + ',' + str(RefAntOut[1]) + ',' + \
                 str(RefAntOut[2]) + ',' + str(RefAntOut[3])
    refantenna = refantenna.upper()
writelog("")
writelog("Reference antenna(s) being used: " + refantenna)

# p-gaincal (short solint) w/ phase calibrator
writelog("short solint p-gaincal with phase calibrator ...")
rmtables(short_p)
gaincal(vis = calsMS,
        caltable = short_p,
        field = phasefield,
        gaintype = 'G',
        calmode = 'p',
        spw = gaincalspw,
        minsnr = 2.5,
        solint = init_solint,
        solmode = 'L1R',
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
flaggedSolnResult = getCalFlaggedSoln(short_p)
writelog( "  Fraction of flagged solutions = " + \
         str(flaggedSolnResult['all']['fraction']) )
writelog( "  Median fraction of flagged solutions per antenna = " + \
         str(flaggedSolnResult['antmedian']['fraction']) )
plotfilepath = logsfolder + '03-0-shortp_phas-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = short_p,
       xaxis = 'time',
       yaxis = 'phase',
       xselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       plotrange = [-1,-1,-180,180],
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')

# ap-gaincal (long solint) w/ phase calibrator
writelog("long solint ap-gaincal with phase calibrator ...")
rmtables(long_ap)
gaincal(vis = calsMS,
        caltable = long_ap,
        field = phasefield,
        gaintype = 'G',
        calmode = 'ap',
        spw = gaincalspw,
        minsnr = 3.0,
        solint = main_solint,
        solmode = 'L1R',
        gaintable = [short_p],
        interp    = ['nearest'],
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
flaggedSolnResult = getCalFlaggedSoln(long_ap)
writelog( "  Fraction of flagged solutions = " + \
         str(flaggedSolnResult['all']['fraction']) )
writelog( "  Median fraction of flagged solutions per antenna = " + \
         str(flaggedSolnResult['antmedian']['fraction']) )
plotfilepath = logsfolder + '03-1-longap_phas-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = long_ap,
       xaxis = 'time',
       yaxis = 'phase',
       xselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       plotrange = [-1,-1,-180,180],
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')
plotfilepath = logsfolder + '03-1-longap_amp-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = long_ap,
       xaxis = 'time',
       yaxis = 'amp',
       xselfscale = True,
       yselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')

# apply on phase calibrator, flag corrected data, save flags, write summary
writelog("Applying solutions on phase calibrator ...")
applycal(vis = calsMS,
         field = phasefield,
         applymode = 'calflagstrict',
         gaintable = [short_p, long_ap],
         interp    = ['nearest','nearest'],
         calwt = False,
         flagbackup = False,
         parang = True)
writelog("Flagging ...")
flagdata(vis = calsMS,
         mode = 'tfcrop',
         field = phasefield,
         datacolumn = 'corrected',
         timefit = 'line',
         freqfit = 'line',
         timecutoff = 4.5,
         freqcutoff = 4.5,
         maxnpieces = 5,
         #usewindowstats = 'both', halfwin = 2,          # EXPERIMENTAL
         extendflags = False,
         flagbackup = False)
flagmanager(vis = calsMS,
            mode = 'save',
            versionname = 'aftergains',
            comment = 'after 2nd applycal in 3-getGains.py',
            merge = 'replace')
flagsummarywrite(calsMS, fields=phasefield)

# set reference antenna using heuristics if not being forced by user
if forceantenna==False:
    findrefant = RefAntHeuristics(vis=calsMS, field=allcals, \
                                  geometry=True, flagging=True)
    RefAntOut  = findrefant.calculate()
    refantenna = str(RefAntOut[0]) + ',' + str(RefAntOut[1]) + ',' + \
                 str(RefAntOut[2]) + ',' + str(RefAntOut[3])
    refantenna = refantenna.upper()
writelog("")
writelog("Reference antenna(s) being used: " + refantenna)

# p-gaincal (short solint) w/ phase+flux calibrators
writelog("short solint p-gaincal with flux and phase calibrators ...")
rmtables(short_p)
gaincal(vis = calsMS,
        caltable = short_p,
        field = phasefield+','+fluxfield,
        gaintype = 'G',
        calmode = 'p',
        spw = gaincalspw,
        minsnr = 2.5,
        solint = init_solint,
        solmode = 'L1R',
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
flaggedSolnResult = getCalFlaggedSoln(short_p)
writelog( "  Fraction of flagged solutions = " + \
         str(flaggedSolnResult['all']['fraction']) )
writelog( "  Median fraction of flagged solutions per antenna = " + \
         str(flaggedSolnResult['antmedian']['fraction']) )
plotfilepath = logsfolder + '03-2-shortp_phas-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = short_p,
       xaxis = 'time',
       yaxis = 'phase',
       xselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       plotrange = [-1,-1,-180,180],
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')

# plot phase stability check: if this isn't good, may need to change refant
plotfilepath = logsfolder + '03-2-shortp_stability-time.png'
writelog("Plotting " + plotfilepath + " ...")
plotms(vis = short_p,
       xaxis = 'time',
       yaxis = 'phase',
       correlation = '/',
       coloraxis = 'baseline',
       plotfile = plotfilepath,
       showgui = False,
       overwrite = True)

# ap-gaincal (long solint) w/ phase+flux calibrators
writelog("long solint ap-gaincal with flux and phase calibrators ...")
rmtables(long_ap)
gaincal(vis = calsMS,
        caltable = long_ap,
        field = phasefield+','+fluxfield,
        gaintype = 'G',
        calmode = 'ap',
        spw = gaincalspw,
        minsnr = 3.0,
        solint = main_solint,
        solmode = 'L1R',
        gaintable = [short_p],
        interp    = ['nearest'],
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
flaggedSolnResult = getCalFlaggedSoln(long_ap)
writelog( "  Fraction of flagged solutions = " + \
         str(flaggedSolnResult['all']['fraction']) )
writelog( "  Median fraction of flagged solutions per antenna = " + \
         str(flaggedSolnResult['antmedian']['fraction']) )
plotfilepath = logsfolder + '03-3-longap_phas-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = long_ap,
       xaxis = 'time',
       yaxis = 'phase',
       xselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       plotrange = [-1,-1,-180,180],
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')
plotfilepath = logsfolder + '03-3-longap_amp-time.png'
writelog("  Plotting " + plotfilepath + " ...")
plotms(vis = long_ap,
       xaxis = 'time',
       yaxis = 'amp',
       xselfscale = True,
       yselfscale = True,
       gridrows = 4,
       gridcols = 3,
       width = 1500,
       height = 900,
       iteraxis = 'antenna',
       coloraxis = 'corr',
       overwrite = True,
       showgui = False,
       plotfile = plotfilepath,
       exprange = 'all')

writelog("Scaling the amplitude solutions ...")
rmtables(scaled_fx)
calfluxes = fluxscale(vis = calsMS,
                      caltable = long_ap,
                      fluxtable = scaled_fx,
                      reference = fluxfield,
                      transfer = phasefield,
                      incremental = False)
# needs to be more robust. dictionary has different keys some times.
#writelog("  Flux density of phase calibrator(Jy): " +
#         str(int(calfluxes['1']['0']['fluxd'][0]*1e5)/1e5) + '+/-' +
#         str(int(calfluxes['1']['0']['fluxdErr'][0]*1e5)/1e5) )
#writelog("  Spectral index: " +
#         str(int(calfluxes['1']['spidx'][0])) + '+/-' +
#         str(int(calfluxes['1']['spidxerr'][0])) )
#writelog("  Frequency (MHz): "+ str(int(calfluxes['1']['fitRefFreq']/1e6)))
#writelog("  Number of solutions: "+ str(int(calfluxes['1']['0']['numSol'][0])))

# p-gaincal (long solint) w/ all calibrators (applied only on targets)
writelog("long solint p-gaincal with all calibrators ...")
rmtables(long_p)
gaincal(vis = calsMS,
        caltable = long_p,
        field = allcals,
        gaintype = 'G',
        calmode = 'p',
        spw = gaincalspw,
        minsnr = 3.0,
        gaintable = [scaled_fx],
        solint = main_solint,
        solmode = 'L1R',
        uvrange = uvracal,
        refant = refantenna,
        parang = True)
flaggedSolnResult = getCalFlaggedSoln(long_p)
writelog( "  Fraction of flagged solutions = " + \
         str(flaggedSolnResult['all']['fraction']) )
writelog( "  Median fraction of flagged solutions per antenna = " + \
         str(flaggedSolnResult['antmedian']['fraction']) )
plotfilepath = logsfolder + '03-4-longp_stability-time.png'
writelog("Plotting " + plotfilepath + " ...")
plotms(vis = long_p,
       xaxis = 'time',
       yaxis = 'phase',
       correlation = '/',
       coloraxis = 'baseline',
       plotfile = plotfilepath,
       showgui = False,
       overwrite = True)

# for diagnostic purposes, apply final tables on this MS and make plots
applycal(vis = calsMS,
         applymode = 'calflagstrict',
         gaintable = [short_p, scaled_fx],
         interp    = ['nearest','nearest'],
         calwt = False,
         flagbackup = False,
         parang = True)
flagmanager(vis = calsMS,
            mode = 'save',
            versionname = 'aftergains2',
            comment = 'after 3rd applycal in 3-getGains.py',
            merge = 'replace')
writelog("Making plots of calibrators...")
for fld in allcals.split(','):
    for yax in ['amp','phase']:
        plotfilepath = logsfolder + '03-5-'+yax+'_UV-'+fld+'.png'
        writelog("Plotting " + plotfilepath + " ...")
        plotms( vis = calsMS,
                xaxis = 'UVwave',
                yaxis = yax,
                ydatacolumn = 'corrected',
                coloraxis = 'antenna1',
                field = fld,
                correlation = 'RR,LL',
                averagedata = True,
                avgtime = '1e8',
                avgchannel = '1e8',
                scalar = True,
                overwrite = True,
                showgui = False,
                plotfile = plotfilepath,
                exprange = 'all' )
for yax in ['amp','phase']:
    plotfilepath = logsfolder + '03-6-'+yax+'_time.png'
    writelog("Plotting " + plotfilepath + " ...")
    plotms( vis = calsMS,
            xaxis = 'time',
            yaxis = yax,
            xselfscale = True,
            yselfscale = True,
            ydatacolumn = 'corrected',
            field = allcals,
            coloraxis = 'field',
            iteraxis = 'baseline',
            correlation = 'RR,LL',
            averagedata = True,
            avgspw = True,
            avgchannel = '1e8',
            scalar = True,
            antenna = refantenna,
            width = 1500,
            height = 900,
            gridrows = 4,
            gridcols = 3,
            overwrite = True,
            showgui = False,
            plotfile = plotfilepath,
            exprange = 'all')


writelog("")
writelog("-- Gain tables are ready --")

# If self-cal is yet to be done to get a model of the phase cal field,
# ... don't go to 4-calibrateTargets.py!
# Apply solutions on phase calibrator data, split corrected data, and then
# ... self-calibrate using the script in selfcalPhase.py

