#  PLEASE READ THE FILE 'README' IN THE FOLDER BEFORE CALIBRATING!

import numpy as np, datetime, os, subprocess, time, glob, csv
from copy import deepcopy as dcp
from scipy.special import j1
from astropy.io import fits
from astropy import units as u
from astropy.coordinates import SkyCoord

# create log folder that would contain plots and the log text file
try:
    print("Logs folder: " + logsfolder)
except:
    print("No logs folder specified.")
    logsfolder='./'
if logsfolder=='.' or logsfolder=='./':
    print("Plots will be saved in the current folder.")
else:
    if os.path.isdir(logsfolder)==True:
        print("The folder: " + logsfolder + " exists. Files may be overwritten.")
    else:
        subprocess.Popen(['mkdir', logsfolder])
        time.sleep(5)       # giving enough time to create the folder
        if os.path.isdir(logsfolder)==False:
            sys.exit("Unable to create the logs folder. Please do it manually and re-run.")
        else:
            print("Folder created successfully.")
if os.path.exists(pipe_log)==True:
    print("The file: " + pipe_log + " exists. File will be appended with the log.")
else:
    subprocess.Popen(['touch', pipe_log])
    time.sleep(5)       # giving enough time to create the file
    if os.path.exists(pipe_log)==False:
        sys.exit("Unable to create the log file. Please do it manually and re-run.")
    else:
        print("Log file created successfully.")

# Try to define calibration table and other variable names.
# If unsuccessful, just mention this and proceed.
try:
    p_delay   = myms + ".K.p"                                                       # calibration tables
    delays    = myms + ".K"
    ap_bpass  = myms + ".BP.ap"
    bpass     = myms + ".BP"
    avg_p     = myms + ".avg.p"
    pol_ap    = myms + ".pol.ap"
    pol_fx    = myms + ".pol.fx"
    pol_KCR   = myms + ".pol.KCR"
    pol_leak  = myms + ".pol.Df"
    pol_PA    = myms + ".pol.Xf"
    short_p   = myms + ".short.p"
    long_ap   = myms + ".long.ap"
    long_p    = myms + ".long.p"
    scaled_fx = myms + ".scaled.fx"
except:
    print("\nSkipped defining calibration table names.\n")
try:
    if polcalibTF==True:
        DBPlist = bpassfield +','+ delayfield +','+ polcalib +','+ unpolcalib       # BP, delay and polcal calibrator fields
        DBPcals = ','.join(np.unique(DBPlist.split(',')))
    else:
        DBPlist = bpassfield +','+ delayfield                                       # BP and delay calibrator fields
        DBPcals = ','.join(np.unique(DBPlist.split(',')))
    allcalslist = fluxfield +','+ phasefield +','+ DBPcals                          # all calibrator fields unique list
    allcals = ','.join(np.unique(allcalslist.split(',')))
    allfieldslist = allcals +','+ targfields                                        # all fields unique list
    allfields = ','.join(np.unique(allfieldslist.split(',')))
    calsMS = myms[:-3] + '_cals.ms'
    DBPcalsMS = myms[:-3] + '_DBPcals.ms'
    polcalsMS = myms[:-3] + '_polcals.ms'
except:
    print("\nSkipped defining field names.\n")

# function to write to log along with current time
def writelog(logstring):
    logfile = open(pipe_log, "a+", 0)
    out_str = datetime.datetime.now().strftime("[%Y-%m-%d__%H:%M:%S]  ")  \
              + logstring + '\n'
    logfile.write(out_str)
    logfile.close()
writelog("---------------------------------- ")
writelog("   GMRT data reduction pipeline    ")
writelog(" Developed by Rohit Dokara @ MPIfR ")
writelog("     version: 2021.07_5.8.0v4      ")
writelog("---------------------------------- ")

# function to get and print flag summary
def flagsummarywrite(msname,fields='',spaces=''):
    flagsummary = flagdata(vis=msname, mode='summary', field=fields)
    for fldID in flagsummary['field']:
        flddata = flagsummary['field'][fldID]
        flagpercent = str(100*flddata['flagged']/flddata['total'])[:4]
        writelog(spaces + "Flagging in " + fldID + ": " + flagpercent + "%")

# sigma clipping to get mean or RMS
def s_clip( data, niter=20, nsigma=3 ):
    ndata = dcp(data)
    for i in range(niter+1):
        mean = np.nanmean(ndata)
        std  = np.nanstd(ndata)
        lim1 = mean - (nsigma*std)
        lim2 = mean + (nsigma*std)
        ndata[ (ndata>lim2) + (ndata<lim1) ] = np.nan
    return [mean, std]

# function to get PB attenuation factor
def pbatt( angsep, frequ, dishdia ):
    '''
    Give angsep in degree, dishdia in m, and frequ in Hz
    '''
    x = 2*np.pi*frequ/3e8 * dishdia/2 * np.sin(angsep*np.pi/180)
    return (2*j1(x)/x)**2

# function to get outliers to be imaged
def getoutliers( imphcent, imsiz, frequg, minflux, maxnout, outlfldr, outlrnm,
                 catfile = '/msn2e2/rdokara/other-survey-data/NVSS-TGSS-spidxcat_v1.1b.fits',
                 extsrcs = '/homes/rdokara/uGMRT/5.8.0.v3/imaging/extsrcs.txt',
                 maxdist = 1.4,
                 lspidx  = -0.7 ):
    '''
    imphcent  COORD phase center of the image in SkyCoord format
    imsiz     DEG   size of image
    frequg    Hz    frequency of the GMRT data
    minflux   Jy    minimum FD of outlier as seen by the dish
    maxnout   -     maximum number of outliers
    outlfldr  PATH  folder in which outliers are imaged
    outlrnm   PATH  filename of outliers file to give to CASA
    catfile   PATH  TGSS-NVSS spidx catalog by de Gasperin et al 2018
    extsrcs   PATH  list of bright extended sources (no header, comma separated: RA_deg,dec_deg,radius_arcsec)
    maxdist   DEG   maximum distance of an outlier
    lspidx    -     spidx replacement for limits (used only if lowerlimit<lspidx or upperlimit>lspidx)
    '''
    freqnv  = 1410e6        # [Hz]  frequency of NVSS survey
    gmrtdia = 45.           # [m] diameter of GMRT dish

    # get data from the catalog, add a column of sizes of 60 arcsec to it
    hdul = fits.open(catfile)
    dta  = hdul[1].data.columns.del_col('E_Total_flux_NVSS').del_col('Peak_flux_NVSS').del_col('E_Peak_flux_NVSS').del_col('E_Total_flux_TGSS').del_col('Peak_flux_TGSS').del_col('E_Peak_flux_TGSS').del_col('Rms_NVSS').del_col('Rms_TGSS').del_col('E_Spidx').del_col('Num_match').del_col('Num_unmatch_NVSS').del_col('Num_unmatch_TGSS').del_col('Isl_id').del_col('Source_id').del_col('s2n')     # delete unnecessary columns
    newc = fits.ColDefs([fits.Column( name='SIZE', format='D', array=np.zeros(len(hdul[1].data))+60 )])     # add size column. all have 60 arcsec
    hdu  = fits.BinTableHDU.from_columns(dta+newc)
    dat1 = hdu.data

    # get extended sources
    with open(extsrcs, 'r') as csvfil:
        my_reader = csv.reader(csvfil, delimiter=',')
        csvdata = []
        for row in my_reader:
            csvdata.append( [float(itm) for itm in row] )

    # save extended sources and catalog sources in a table
    newtabl = fits.BinTableHDU.from_columns( dat1.columns, nrows=len(dat1)+len(csvdata) ).data
    for idx in range( len(dat1), len(newtabl) ):
        newtabl[idx]['RA']  = csvdata[idx-len(dat1)][0]
        newtabl[idx]['dec'] = csvdata[idx-len(dat1)][1]
        newtabl[idx]['SIZE'] = csvdata[idx-len(dat1)][2]
        newtabl[idx]['Total_flux_NVSS'] = 1e4       # some large number to make sure it is always included in the outliers list if they're nearby
        newtabl[idx]['Total_flux_TGSS'] = 1e4
        newtabl[idx]['Spidx'] = 0.
        newtabl[idx]['S_code'] = 'I'

    # get the sources inside the required annulus
    locs_all = SkyCoord(newtabl['RA']*u.degree,newtabl['dec']*u.degree,frame='icrs')
    cond0 = (locs_all.separation(imphcent).deg) < maxdist
    cond1 = locs_all.ra.deg  < (imphcent.ra.deg -imsiz/2)
    cond2 = locs_all.ra.deg  > (imphcent.ra.deg +imsiz/2)
    cond3 = locs_all.dec.deg < (imphcent.dec.deg-imsiz/2)
    cond4 = locs_all.dec.deg > (imphcent.dec.deg+imsiz/2)
    srcs_ann = newtabl[ cond0*(cond1+cond2+cond3+cond4) ]

    # measure their flux densities and save required info
    srcs_loc = SkyCoord(srcs_ann['RA']*u.degree,srcs_ann['dec']*u.degree,frame='icrs')
    srcs_fd  = np.zeros(len(srcs_loc))
    for idx in range(len(srcs_loc)):
        # take care of lower and upper limits first
        if srcs_ann['S_code'][idx]=='L':
            spdx = np.max([srcs_ann['Spidx'][idx],lspidx])
        elif srcs_ann['S_code'][idx]=='U':
            spdx = np.min([srcs_ann['Spidx'][idx],lspidx])
        else:
            spdx = srcs_ann['Spidx'][idx]
        # get source flux density considering PB attenuation
        srcs_fd[idx]  = srcs_ann['Total_flux_NVSS'][idx] * ((frequg/freqnv)**spdx) * \
                        pbatt( srcs_loc[idx].separation(imphcent).deg, frequg, gmrtdia )

    # sort based on flux densities
    sortix = np.argsort(srcs_fd)
    f_locs = srcs_loc[sortix][::-1]
    f_fds  = srcs_fd[sortix][::-1]

    # write the outlier file for the brightest sources
    outstr = ''
    n = 0   # to keep track of number of outliers and also outlier image names
    for idx in range(len(f_locs)):
        if f_fds[idx]>minflux and n<maxnout:
            n=n+1
            coordstr = f_locs[idx].to_string('hmsdms').replace('s','').replace('h',':').replace('d','.').replace('m',':',1).replace('m','.')
            outstr = outstr + 'imagename='+outlfldr+'o'+str(n) + '\n' \
                        + 'imsize=150\ndeconvolver=hogbom\n' \
                        + 'phasecenter=J2000 ' + coordstr + '\n\n'
    if os.path.isdir(outlfldr)==False:
        subprocess.Popen(['mkdir', outlfldr])
        time.sleep(1)       # giving enough time to create the folder
    writefile = open(outlrnm, "w", 0)
    writefile.write(outstr)
    writefile.close()
    time.sleep(1)       # giving enough time to create the file

    if n==0:        # use no file if there are no outliers
        return ''
    else:
        return outlrnm

# get beam size of the image of a given MS and field
def getbeam( vis, field='', datacolumn='data', cell='1arcsec', imsize=100,
             weighting='briggs', robust=0.5, uvtaper='',
             parallel=True, uvrange='', gridder='widefield', wprojplanes=-1,
             specmode='mfs', deconvolver='mtmfs', nterms=2 ):
    tclean( vis         = vis,
            imagename   = 'beamchc.'+vis,
            datacolumn  = datacolumn,
            field       = field,
            cell        = cell,
            weighting   = weighting,
            robust      = robust,
            parallel    = parallel,
            imsize      = imsize,
            uvrange     = uvrange,
            uvtaper     = uvtaper,
            specmode    = specmode,
            deconvolver = deconvolver,
            nterms      = nterms,
            gridder     = gridder,
            wprojplanes = wprojplanes,
            interactive = False,
            restart     = False,
            pblimit     = -0.01,
            niter       = 0,
            savemodel   = 'none',
            calcpsf     = True,
            calcres     = True )
    imhd = imhead('beamchc.'+vis+'.image.tt0')
    bmaj = imhd['restoringbeam']['major']['value']
    bmin = imhd['restoringbeam']['minor']['value']
    bpa  = imhd['restoringbeam']['positionangle']['value']
    #os.system('rm -rf ./beamchc.'+vis+'*')
    return [bmaj,bmin,bpa]      # assuming they are in arcsec, arcsec, degree

# estimate the tapering which roughly circularizes the synthesized beam
def taper2circular( vis, field='', datacolumn='data', cell='1arcsec', imsize=100,
                    weighting='briggs', robust=0.5, uvtaper='',
                    parallel=True, uvrange='', gridder='widefield', wprojplanes=-1,
                    specmode='mfs', deconvolver='mtmfs', nterms=2 ):
    '''
    will not work if there are large holes in the uv coverage
    '''
    oldbeam = getbeam( vis, field=field, datacolumn=datacolumn, cell=cell, imsize=imsize,
                       weighting=weighting, robust=robust, uvtaper=uvtaper,
                       parallel=parallel, uvrange=uvrange, gridder=gridder, wprojplanes=wprojplanes,
                       specmode=specmode, deconvolver=deconvolver, nterms=nterms )
    bmaj = np.sqrt(oldbeam[0]**2 - oldbeam[1]**2)
    bmin = 0.5          # just a small value
    bpa  = 90. + oldbeam[2]
    return [bmaj,bmin,bpa]

# to work in a particular directory
class workindir:
    """
    Change the current working directory and return after task.  Example:
    with workindir("~/Library"):
        # we are in ~/Library
        subprocess.call("ls")
    """
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


################################################################################
# From here, it's not my own work.  All the below are copied from VLA pipeline!
# See science.nrao.edu/facilities/vla/data-processing/pipeline/scripted-pipeline

# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# findrefant.py
#   This file contains the reference antenna heuristics.
#   The present heuristics are geometry and flagging.
#   Classes:
#     RefAntHeuristics - This class chooses the reference antenna heuristics.
#     RefAntGeometry   - This class contains the geometry heuristics for the
#                        reference antenna.
#     RefAntFlagging   - This class contains the flagging heuristics for the
#                        reference antenna.
#   Modification history:
#   2012 May 21 - Nick Elias, NRAO
#                 Initial version.
#   2012 Jun 06 - Nick Elias, NRAO
#                 Modified to exclude ALMA pipeline classe dependencies.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# class RefAntHeuristics
# This class chooses the reference antenna heuristics.
# Public member variables:
# vis      - This python string contains the MS name.
# field    - This python string or list of strings contains the field numbers
#            or IDs.  Presently it is used only for the flagging heuristic.
# spw      - This python string or list of strings contains the spectral
#            window numbers of IDs.  Presently it is used only for the
#            flagging heuristic.
# intent   - This python string or list of strings contains the intent(s).
#            Presently it is used only for the flagging heuristic.
# geometry - This python boolean determines whether the geometry heuristic will
#            be used.
# flagging - This python boolean determines whether the flagging heuristic will
#            be used.
# Public member functions:
# __init__  - This public member function constructs an instance of the
#             RefAntHeuristics() class.
# calculate - This public member function forms the reference antenna list
#             calculated from the selected heuristics.
# Private member functions:
# _get_names - This private member function gets the antenna names from the MS.
# Modification history:
# 2012 May 21 - Nick Elias, NRAO
#               Initial version created with public member variables vis, field,
#               spw, intent, geometry, and flagging; public member functions
#               __init__() and calculate(); and private member function
#               _get_names().
# 2012 Jun 06 - Nick Elias, NRAO
#               api inheritance eliminated.
# ------------------------------------------------------------------------------
class RefAntHeuristics:
# NB: If all of the defaults are chosen, no reference antenna list is returned.
# Outputs: None, returned via the function value.
# Modification history:
# 2012 May 21 - Nick Elias, NRAO
#               Initial version.
# 2012 Jun 06 - Nick Eluas, NRAO
#               Input parameter defaults added.
    def __init__( self, vis, field='', spw='', intent='', geometry=False,
        flagging=False ):
        # Initialize the public member variables of this class
        self.vis = vis
        self.field = field
        self.spw = spw
        self.intent = intent
        self.geometry = geometry
        self.flagging = flagging
        return None
    def calculate( self ):
        '''
        This public member function forms the reference antenna list
        calculated from the selected heuristics.
        Inputs: None.
        Outputs: The numpy array of strings containing the ranked reference
        antenna list returned via the function value.
        '''
        # If no heuristics are specified, return no reference antennas
        if not ( self.geometry or self.flagging ): return []
        # Get the antenna names and initialize the score dictionary
        names = self._get_names()
        score = dict()
        for n in names: score[n] = 0.0
        # For each selected heuristic, add the score for each antenna
        self.geoScore = 0.0
        self.flagScore = 0.0
        if self.geometry:
            geoClass = RefAntGeometry( self.vis )
            self.geoScore = geoClass.calc_score()
            for n in names: score[n] += self.geoScore[n]
        if self.flagging:
            flagClass = RefAntFlagging( self.vis, self.field,
                self.spw, self.intent )
            self.flagScore = flagClass.calc_score()
            for n in names:
                try:
                    score[n] += self.flagScore[n]
                except KeyError, e:
                    #writelog("Antenna "+str(e)+" is completely flagged.")
                    pass
        # Calculate the final score and return the list of ranked
        # reference antennas.  NB: The best antennas have the highest
        # score, so a reverse sort is required.
        keys = np.array( score.keys() )
        values = np.array( score.values() )
        argSort = np.argsort( values )[::-1]
        refAntUpper = keys[argSort]
        refAnt = list()
        for r in refAntUpper: refAnt.append( r.lower() )
        return( refAnt )
    def _get_names( self ):
        '''
        This private member function gets the antenna names from the MS.
        Inputs: None.
        Outputs: The numpy array of strings containing the antenna names,
                returned via the function value.
        '''
        # Create the local instance of the table tool and open the MS
        tbLoc = casac.table()
        tbLoc.open( self.vis+'/ANTENNA' )
        # Get the antenna names and capitalize them (unfortunately,
        # some CASA tools capitalize them and others don't)
        names = tbLoc.getcol( 'NAME' ).tolist()
        rNames = range( len(names) )
        for n in rNames: names[n] = names[n]
        # Close the local instance of the table tool and delete it
        tbLoc.close()
        del tbLoc
        return names

# ------------------------------------------------------------------------------
# class RefAntGeometry
# This class contains the geometry heuristics for the reference antenna.
# Algorithm:
# * Calculate the antenna distances from the array center.
# * Normalize the distances by the maximum distance.
# * Calculate the score for each antenna, which is one minus the normalized
#   distance.  The best antennas have the highest score.
# * Sort according to score.
# Public member variables:
# vis - This python string contains the MS name.
# Public member functions:
# __init__   - This public member function constructs an instance of the
#              RefAntGeometry() class.
# calc_score - This public member function calculates the geometry score for
#              each antenna.
# Private member functions:
# _get_info       - This private member function gets the information from the
#                   antenna table of the MS.
# _get_measures   - This private member function gets the measures from the
#                   antenna table of the MS.
# _get_latlongrad - This private member function gets the latitude, longitude
#                   and radius (from the center of the earth) for each antenna.
# _calc_distance  - This private member function calculates the antenna
#                   distances from the array reference from the radii,
#                   longitudes, and latitudes.
# _calc_score     - This private member function calculates the geometry score
#                   for each antenna.
# ------------------------------------------------------------------------------
class RefAntGeometry:
    def __init__( self, vis ):
        # Set the public variables
        self.vis = vis
        return None
    def calc_score( self ):
        '''
        This public member function calculates the geometry score for each
        antenna.
        Inputs: None.
        Outputs: The python dictionary containing the score for each antenna,
        returned via the function value.
        '''
        # Get the antenna information, measures, and locations
        info = self._get_info()
        measures = self._get_measures( info )
        radii, longs, lats = self._get_latlongrad( info, measures )
        # Calculate the antenna distances and scores
        distance = self._calc_distance( radii, longs, lats )
        score = self._calc_score( distance )
        return score
    def _get_info( self ):
        '''
        This private member function gets the information from the antenna
        table of the MS.
        Inputs: None.
        Outputs: The python dictionary containing the antenna information, returned
                    via the function value.
        The dictionary format is:
        'position'          - numpy array of antenna positions.
        'flag_row'          - numpy array of booleans of the flag row booleans.
                              NB: This element is of limited use now and
                              may be eliminated.
        'name'              - numpy array of strings of the antenna names.
        'position_keywords' - This python dictionary of the antenna information.
        '''
        # Create the local instance of the table tool and open it with
        # the antenna subtable of the MS
        tbLoc = casac.table()
        tbLoc.open( self.vis+'/ANTENNA' )
        # Get the antenna information from the antenna table
        info = dict()
        info['position'] = tbLoc.getcol( 'POSITION' )
        info['flag_row'] = tbLoc.getcol( 'FLAG_ROW' )
        info['name'] = tbLoc.getcol( 'NAME' )
        info['position_keywords'] = tbLoc.getcolkeywords( 'POSITION' )
        # Close the table tool and delete the local instance
        tbLoc.close()
        del tbLoc
        # The flag tool appears to return antenna names as upper case,
        # which seems to be different from the antenna names stored in
        # MSes.  Therefore, these names will be capitalized here.
        rRow = range( len( info['name'] ) )
        return info
    def _get_measures( self, info ):
        '''
        private member function to get measures from antenna table of the MS.
        Inputs: info - python dict of antenna info from _get_info().
        Outputs: dictionary of antenna measures, returned via function value.
                    keys: '<antenna name>'
        '''
        # Create the local instances of the measures and quanta tools
        meLoc = casac.measures()
        qaLoc = casac.quanta()
        # Initialize the measures dictionary and the position and
        # position_keywords variables
        measures = dict()
        position = info['position']
        position_keywords = info['position_keywords']
        rf = position_keywords['MEASINFO']['Ref']
        for row,ant in enumerate( info['name'] ):
            if not info['flag_row'][row]:
                p = position[0,row]
                pk = position_keywords['QuantumUnits'][0]
                v0 = qaLoc.quantity( p, pk )
                p = position[1,row]
                pk = position_keywords['QuantumUnits'][1]
                v1 = qaLoc.quantity( p, pk )
                p = position[2,row]
                pk = position_keywords['QuantumUnits'][2]
                v2 = qaLoc.quantity( p, pk )
                measures[ant] = meLoc.position( rf=rf, v0=v0,
                    v1=v1, v2=v2 )
        # Delete the local instances of the measures and quanta tools
        del qaLoc
        del meLoc
        return measures
    def _get_latlongrad( self, info, measures ):
        '''
        gets the latitude, longitude and radius (from the center of the earth)
        for each antenna.
        Inputs:
        # info     - antenna information from _get_info().
        # measures - antenna measures from _get_measures().
        Outputs: The python tuple containing containing radius, longitude, and
                    latitude dictionaries, returned via the function value.
        '''
        # Create the local instance of the quanta tool
        qaLoc = casac.quanta()
        # Get the radii, longitudes, and latitudes
        radii = dict()
        longs = dict()
        lats = dict()
        for ant in info['name']:
            value = measures[ant]['m2']['value']
            unit = measures[ant]['m2']['unit']
            quantity = qaLoc.quantity( value, unit )
            convert = qaLoc.convert( quantity, 'm' )
            radii[ant] = qaLoc.getvalue( convert )
            value = measures[ant]['m0']['value']
            unit = measures[ant]['m0']['unit']
            quantity = qaLoc.quantity( value, unit )
            convert = qaLoc.convert( quantity, 'rad' )
            longs[ant] = qaLoc.getvalue( convert )
            value = measures[ant]['m1']['value']
            unit = measures[ant]['m1']['unit']
            quantity = qaLoc.quantity( value, unit )
            convert = qaLoc.convert( quantity, 'rad' )
            lats[ant] = qaLoc.getvalue( convert )
        # Delete the local instance of the quanta tool
        del qaLoc
        return radii, longs, lats
    def _calc_distance( self, radii, longs, lats ):
        # Convert the dictionaries to numpy float arrays.  The median
        # longitude is subtracted.
        radiusValues = np.array( radii.values() )
        longValues = np.array( longs.values() )
        longValues -= np.median( longValues )
        latValues = np.array( lats.values() )
        # Calculate the x and y antenna locations.  The medians are subtracted.
        x = longValues * np.cos(latValues) * radiusValues
        x -= np.median( x )
        y = latValues * radiusValues
        y -= np.median( y )
        # Calculate the antenna distances from the array reference
        distance = dict()
        names = radii.keys()
        for i,ant in enumerate(names):
            distance[ant] = np.sqrt( pow(x[i],2) + pow(y[i],2) )
        return distance
    def _calc_score( self, distance ):
        # Get the number of good data, calculate the fraction of good
        # data, and calculate the good and bad weights
        far = np.array( distance.values(), np.float )
        fFar = far / float( np.max(far) )
        wFar = fFar * len(far)
        wClose = ( 1.0 - fFar ) * len(far)
        # Calculate the score for each antenna and return them
        score = dict()
        names = distance.keys()
        rName = range( len(wClose) )
        for n in rName: score[names[n]] = wClose[n]
        return score

# ------------------------------------------------------------------------------
# RefAntFlagging
# This class contains the flagging heuristics for the reference antenna.
# Algorithm:
# * Get the number of unflagged (good) data for each antenna.
# * Normalize the good data by the maximum good data.
# * Calculate the score for each antenna, which is one minus the normalized
#   number of good data.  The best antennas have the highest score.
# * Sort according to score.
# Public member variables:
# vis    - This python string contains the MS name.
# field  - This python string or list of strings contains the field numbers or
#          or IDs.
# spw    - This python string or list of strings contains the spectral window
#          numbers of IDs.
# intent - This python string or list of strings contains the intent(s).
# Public member functions:
# __init__   - This public member function constructs an instance of the
#              RefAntFlagging() class.
# calc_score - This public member function calculates the flagging score for
#              each antenna.
# Private member functions:
# _get_good   - This private member function gets the number of unflagged (good)
#               data from the MS.
# _calc_score - This private member function calculates the flagging score for
#               each antenna.
# Modification history:
# 2012 May 21 - Nick Elias, NRAO. Initial version.
# ------------------------------------------------------------------------------
class RefAntFlagging:
    def __init__( self, vis, field, spw, intent ):
        # Set the public member functions
        self.vis = vis
        self.field = field
        self.spw = spw
        self.intent = intent
        return None
    def calc_score( self ):
        '''
        # public member function to get the flagging score for each antenna.
        # Inputs: None.
        # Outputs: dict with score for each ant, returned via function value.
        '''
        # Calculate the number of unflagged (good) measurements for each
        # antenna, determine the score, and return them
        good = self._get_good()
        score = self._calc_score( good )
        return( score )
    def _get_good( self ):
        '''
        # private member function to get the number of good data from the MS.
        # returned via the function value.
        '''
        # Update April 2015 to use the flagging task
        task_args = {'vis'         : self.vis,
                    'mode'         : 'summary',
                    'field'        : self.field,
                    'spw'          : self.spw,
                    'intent'       : self.intent,
                    'display'      : '',
                    'flagbackup'   : False,
                    'savepars'     : False}
        d = flagdata(**task_args)
        # Calculate the number of good data for each antenna and return
        antenna = d['antenna']
        good = dict()
        for a in antenna.keys():
            good[a] = antenna[a]['total'] - antenna[a]['flagged']
        return( good )
    def _calc_score( self, good ):
        '''
        # private member function to get the flagging score for each antenna.
        # Algorithm:
        # * Get the number of unflagged (good) data for each antenna.
        # * Normalize the good data by the maximum good data.
        # * Calculate the score for each antenna, (one minus the normalized
        #   number of good data).  The best antennas have the highest score.
        # * Sort according to score.
        # Inputs: good - _get_good() output
        # Outputs: dictionary with score for each antenna, returned via the
        # function value.
        '''
        # Get the number of good data, calculate the fraction of good
        # data, and calculate the good and bad weights
        nGood = np.array( good.values(), np.float )
        fGood = nGood / float( np.max(nGood) )
        wGood = fGood * len(nGood)
        wBad = ( 1.0 - fGood ) * len(nGood)
        # Calculate the score for each antenna and return them
        score = dict()
        names = good.keys()
        rName = range( len(wGood) )
        for n in rName: score[names[n]] = wGood[n]
        return score

# file: lib_pipeutils.py
#  see original VLA pipeline file for details!
def getCalFlaggedSoln(calTable):
    mytb = casac.table()
    import pylab as pl
    mytb.open(calTable)
    antCol = mytb.getcol('ANTENNA1')
    spwCol = mytb.getcol('SPECTRAL_WINDOW_ID')
    fldCol = mytb.getcol('FIELD_ID')
    flagVarCol = mytb.getvarcol('FLAG')
    mytb.close()
    # Initialize a list to hold the results
    rowlist = flagVarCol.keys()
    nrows = len(rowlist)
    # Create the output dictionary
    outDict = {}
    outDict['all'] = {}
    outDict['antspw'] = {}
    outDict['ant'] = {}
    outDict['spw'] = {}
    outDict['antmedian'] = {}
    # Ok now go through and for each row and possibly channel compile flags
    ntotal = 0
    nflagged = 0
    # Lists for median calc
    medDict = {}
    medDict['total'] = []
    medDict['flagged'] = []
    medDict['fraction'] = []
    for rrow in rowlist:
        rown = rrow.strip('r')
        idx = int(rown)-1
        antIdx = antCol[idx]
        spwIdx = spwCol[idx]
        flagArr = flagVarCol[rrow]
        # Get the shape of this data row
        (np,nc,ni) = flagArr.shape
        # ni should be 1 for this
        iid = 0
        # Set up dictionaries if needed
        if outDict['antspw'].has_key(antIdx):
            if not outDict['antspw'][antIdx].has_key(spwIdx):
                outDict['antspw'][antIdx][spwIdx] = {}
                for poln in range(np):
                    outDict['antspw'][antIdx][spwIdx][poln] = {}
                    outDict['antspw'][antIdx][spwIdx][poln]['total'] = 0
                    outDict['antspw'][antIdx][spwIdx][poln]['flagged'] = 0
        else:
            outDict['ant'][antIdx] = {}
            outDict['antspw'][antIdx] = {}
            outDict['antspw'][antIdx][spwIdx] = {}
            for poln in range(np):
                outDict['ant'][antIdx][poln] = {}
                outDict['ant'][antIdx][poln]['total'] = 0
                outDict['ant'][antIdx][poln]['flagged'] = 0.0
                outDict['antspw'][antIdx][spwIdx][poln] = {}
                outDict['antspw'][antIdx][spwIdx][poln]['total'] = 0
                outDict['antspw'][antIdx][spwIdx][poln]['flagged'] = 0.0
        if not outDict['spw'].has_key(spwIdx):
            outDict['spw'][spwIdx] = {}
            for poln in range(np):
                outDict['spw'][spwIdx][poln] = {}
                outDict['spw'][spwIdx][poln]['total'] = 0
                outDict['spw'][spwIdx][poln]['flagged'] = 0.0
        # Sum up the in-row (per pol per chan) flags for this row
        nptotal = 0
        npflagged = 0
        for poln in range(np):
            ntotal += 1
            nptotal += 1
            ncflagged = 0
            for chan in range(nc):
                if flagArr[poln][chan][iid]:
                    ncflagged += 1
            npflagged = float(ncflagged)/float(nc)
            nflagged += float(ncflagged)/float(nc)
            outDict['ant'][antIdx][poln]['total'] += 1
            outDict['spw'][spwIdx][poln]['total'] += 1
            outDict['antspw'][antIdx][spwIdx][poln]['total'] += 1
            outDict['ant'][antIdx][poln]['flagged'] += npflagged
            outDict['spw'][spwIdx][poln]['flagged'] += npflagged
            outDict['antspw'][antIdx][spwIdx][poln]['flagged'] += npflagged
    outDict['all']['total'] = ntotal
    outDict['all']['flagged'] = nflagged
    if ntotal>0:
        outDict['all']['fraction'] = float(nflagged)/float(ntotal)
    else:
        outDict['all']['fraction'] = 0.0
    # Go back and get fractions
    for antIdx in outDict['ant'].keys():
        nptotal = 0
        npflagged = 0
        for poln in outDict['ant'][antIdx].keys():
            nctotal = outDict['ant'][antIdx][poln]['total']
            ncflagged = outDict['ant'][antIdx][poln]['flagged']
            outDict['ant'][antIdx][poln]['fraction'] = float(ncflagged)/float(nctotal)
            nptotal += nctotal
            npflagged += ncflagged
        medDict['total'].append(nptotal)
        medDict['flagged'].append(npflagged)
        medDict['fraction'].append(float(npflagged)/float(nptotal))
    for spwIdx in outDict['spw'].keys():
        for poln in outDict['spw'][spwIdx].keys():
            nptotal = outDict['spw'][spwIdx][poln]['total']
            npflagged = outDict['spw'][spwIdx][poln]['flagged']
            outDict['spw'][spwIdx][poln]['fraction'] = float(npflagged)/float(nptotal)
    for antIdx in outDict['antspw'].keys():
        for spwIdx in outDict['antspw'][antIdx].keys():
            for poln in outDict['antspw'][antIdx][spwIdx].keys():
                nptotal = outDict['antspw'][antIdx][spwIdx][poln]['total']
                npflagged = outDict['antspw'][antIdx][spwIdx][poln]['flagged']
                outDict['antspw'][antIdx][spwIdx][poln]['fraction'] = float(npflagged)/float(nptotal)
    # do medians
    outDict['antmedian'] = {}
    for item in medDict.keys():
        alist = medDict[item]
        aarr = pl.array(alist)
        amed = pl.median(aarr)
        outDict['antmedian'][item] = amed
    outDict['antmedian']['number'] = len(medDict['fraction'])
    return outDict

def getBCalStatistics(calTable,innerbuff=0.1):
    # define range for "inner" channels
    if innerbuff>=0.0 and innerbuff<0.5:
        fcrange = [innerbuff,1.0-innerbuff]
    else:
        fcrange = [0.1,0.9]
    # Print extra information here?
    doprintall = False
    # Create the output dictionary
    outDict = {}
    mytb = casac.table()
    import pylab as pl
    mytb.open(calTable)
    # Check that this is a B Jones table
    caltype = mytb.getkeyword('VisCal')
    if caltype=='B Jones':
        print 'This is a B Jones table, proceeding'
    else:
        print 'This is NOT a B Jones table, aborting'
        return outDict
    antCol = mytb.getcol('ANTENNA1')
    spwCol = mytb.getcol('SPECTRAL_WINDOW_ID')
    fldCol = mytb.getcol('FIELD_ID')
    # these columns are possibly variable in size
    #flagCol = mytb.getcol('FLAG')
    flagVarCol = mytb.getvarcol('FLAG')
    #dataCol = mytb.getcol('CPARAM')
    dataVarCol = mytb.getvarcol('CPARAM')
    mytb.close()
    # get names from ANTENNA table
    mytb.open(calTable+'/ANTENNA')
    antNameCol = mytb.getcol('NAME')
    mytb.close()
    nant = len(antNameCol)
    antDict = {}
    for iant in range(nant):
        antDict[iant] = antNameCol[iant]
    # get names from SPECTRAL_WINDOW table
    mytb.open(calTable+'/SPECTRAL_WINDOW')
    spwNameCol = mytb.getcol('NAME')
    mytb.close()
    nspw = len(spwNameCol)
    # get baseband list
    rxbands = []
    rxBasebandDict = {}
    spwDict = {}
    for ispw in range(nspw):
        try:
            (rx,bb,sb) = spwNameCol[ispw].split('#')
        except:
            rx = 'Unknown'
            bb = 'Unknown'
            sb = ispw
        spwDict[ispw] = {'RX':rx, 'Baseband':bb, 'Subband':sb}
        if rxbands.count(rx)<1:
            rxbands.append(rx)
        if rxBasebandDict.has_key(rx):
            if rxBasebandDict[rx].has_key(bb):
                rxBasebandDict[rx][bb].append(ispw)
            else:
                rxBasebandDict[rx][bb] = [ispw]
        else:
            rxBasebandDict[rx] = {}
            rxBasebandDict[rx][bb] = [ispw]
    print 'Found '+str(len(rxbands))+' Rx bands'
    for rx in rxBasebandDict.keys():
        bblist = rxBasebandDict[rx].keys()
        print 'Rx band '+str(rx)+' has basebands: '+str(bblist)
    # Initialize a list to hold the results
    # Get shape of FLAG
    #(np,nc,ni) = flagCol.shape
    # Get shape of CPARAM
    #(np,nc,ni) = dataCol.shape
    rowlist = dataVarCol.keys()
    nrows = len(rowlist)
    # Populate output dictionary structure
    outDict['antspw'] = {}
    outDict['antband'] = {}
    # Our fields will be: running 'n','mean','min','max' for 'amp'
    # Note - running mean(n+1) = ( n*mean(n) + x(n+1) )/(n+1) = mean(n) + (x(n+1)-mean(n))/(n+1)
    # Ok now go through for each row and possibly channel
    ntotal = 0
    ninner = 0
    nflagged = 0
    ngood = 0
    ninnergood = 0
    for rrow in rowlist:
        rown = rrow.strip('r')
        idx = int(rown)-1
        antIdx = antCol[idx]
        spwIdx = spwCol[idx]
        dataArr = dataVarCol[rrow]
        flagArr = flagVarCol[rrow]
        # Get the shape of this data row
        (np,nc,ni) = dataArr.shape
        # ni should be 1 for this
        iid = 0
        # receiver and baseband and subband
        rx = spwDict[spwIdx]['RX']
        bb = spwDict[spwIdx]['Baseband']
        sb = spwDict[spwIdx]['Subband']
        # Set up dictionaries if needed
        parts = ['all','inner']
        quants = ['amp','phase','real','imag']
        vals = ['min','max','mean','var']
        if outDict['antspw'].has_key(antIdx):
            if not outDict['antspw'][antIdx].has_key(spwIdx):
                outDict['antspw'][antIdx][spwIdx] = {}
                for poln in range(np):
                    outDict['antspw'][antIdx][spwIdx][poln] = {}
                    for part in parts:
                        outDict['antspw'][antIdx][spwIdx][poln][part] = {}
                        outDict['antspw'][antIdx][spwIdx][poln][part]['total'] = 0
                        outDict['antspw'][antIdx][spwIdx][poln][part]['number'] = 0
                        for quan in quants:
                            outDict['antspw'][antIdx][spwIdx][poln][part][quan] = {}
                            for val in vals:
                                outDict['antspw'][antIdx][spwIdx][poln][part][quan][val] = 0.0
            if outDict['antband'][antIdx].has_key(rx):
                if not outDict['antband'][antIdx][rx].has_key(bb):
                    outDict['antband'][antIdx][rx][bb] = {}
                    for part in parts:
                        outDict['antband'][antIdx][rx][bb][part] = {}
                        outDict['antband'][antIdx][rx][bb][part]['total'] = 0
                        outDict['antband'][antIdx][rx][bb][part]['number'] = 0
                        for quan in quants:
                            outDict['antband'][antIdx][rx][bb][part][quan] = {}
                            for val in vals:
                                outDict['antband'][antIdx][rx][bb][part][quan][val] = 0.0
            else:
                outDict['antband'][antIdx][rx] = {}
                outDict['antband'][antIdx][rx][bb] = {}
                for part in parts:
                    outDict['antband'][antIdx][rx][bb][part] = {}
                    outDict['antband'][antIdx][rx][bb][part]['total'] = 0
                    outDict['antband'][antIdx][rx][bb][part]['number'] = 0
                    for quan in quants:
                        outDict['antband'][antIdx][rx][bb][part][quan] = {}
                        for val in vals:
                            outDict['antband'][antIdx][rx][bb][part][quan][val] = 0.0
        else:
            outDict['antspw'][antIdx] = {}
            outDict['antspw'][antIdx][spwIdx] = {}
            for poln in range(np):
                outDict['antspw'][antIdx][spwIdx][poln] = {}
                for part in parts:
                    outDict['antspw'][antIdx][spwIdx][poln][part] = {}
                    outDict['antspw'][antIdx][spwIdx][poln][part]['total'] = 0
                    outDict['antspw'][antIdx][spwIdx][poln][part]['number'] = 0
                    for quan in quants:
                        outDict['antspw'][antIdx][spwIdx][poln][part][quan] = {}
                        for val in vals:
                            outDict['antspw'][antIdx][spwIdx][poln][part][quan][val] = 0.0
            outDict['antband'][antIdx] = {}
            outDict['antband'][antIdx][rx] = {}
            outDict['antband'][antIdx][rx][bb] = {}
            for part in parts:
                outDict['antband'][antIdx][rx][bb][part] = {}
                outDict['antband'][antIdx][rx][bb][part]['total'] = 0
                outDict['antband'][antIdx][rx][bb][part]['number'] = 0
                for quan in quants:
                    outDict['antband'][antIdx][rx][bb][part][quan] = {}
                    for val in vals:
                        outDict['antband'][antIdx][rx][bb][part][quan][val] = 0.0
        # Sum up the in-row (per pol per chan) flags for this row
        nptotal = 0
        npflagged = 0
        for poln in range(np):
            ntotal += 1
            nptotal += 1
            ncflagged = 0
            ncgood = 0
            ncinnergood = 0
            for chan in range(nc):
                outDict['antspw'][antIdx][spwIdx][poln]['all']['total'] += 1
                outDict['antband'][antIdx][rx][bb]['all']['total'] += 1
                fc = 1
                if nc>0:
                    fc = float(chan)/float(nc)
                if fc>=fcrange[0] and fc<fcrange[1]:
                    outDict['antspw'][antIdx][spwIdx][poln]['inner']['total'] += 1
                    outDict['antband'][antIdx][rx][bb]['inner']['total'] += 1
                if flagArr[poln][chan][iid]:
                    # a flagged data point
                    ncflagged += 1
                else:
                    cx = dataArr[poln][chan][iid]
                    # get quantities from complex data
                    ampx = pl.absolute(cx)
                    phasx = pl.angle(cx,deg=True)
                    realx = pl.real(cx)
                    imagx = pl.imag(cx)
                    # put in dictionary
                    cdict = {}
                    cdict['amp'] = ampx
                    cdict['phase'] = phasx
                    cdict['real'] = realx
                    cdict['imag'] = imagx
                    # an unflagged data point
                    ncgood += 1
                    # Data stats
                    # By antspw per poln
                    nx = outDict['antspw'][antIdx][spwIdx][poln]['all']['number']
                    if nx==0:
                        outDict['antspw'][antIdx][spwIdx][poln]['all']['number'] = 1
                        for quan in quants:
                            for val in vals:
                                outDict['antspw'][antIdx][spwIdx][poln]['all'][quan][val] = cdict[quan]
                    else:
                        for quan in quants:
                            vx = cdict[quan]
                            if vx>outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['max']:
                                outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['max'] = vx
                            if vx<outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['min']:
                                outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['min'] = vx
                            # Running mean(n+1) = mean(n) + (x(n+1)-mean(n))/(n+1)
                            meanx = outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['mean']
                            runx = meanx + (vx - meanx)/float(nx+1)
                            outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['mean'] = runx
                            # Running var(n+1) = {n*var(n)+[x(n+1)-mean(n)][x(n+1)-mean(n+1)]}/(n+1)
                            varx = outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['var']
                            qx = nx*varx + (vx-meanx)*(vx-runx)
                            outDict['antspw'][antIdx][spwIdx][poln]['all'][quan]['var'] = qx/float(nx+1)
                    outDict['antspw'][antIdx][spwIdx][poln]['all']['number'] += 1
                    # Now the rx-band stats
                    ny = outDict['antband'][antIdx][rx][bb]['all']['number']
                    if ny==0:
                        outDict['antband'][antIdx][rx][bb]['all']['number'] = 1
                        for quan in quants:
                            for val in vals:
                                outDict['antband'][antIdx][rx][bb]['all'][quan][val] = cdict[quan]
                    else:
                        for quan in quants:
                            vy = cdict[quan]
                            if vy>outDict['antband'][antIdx][rx][bb]['all'][quan]['max']:
                                outDict['antband'][antIdx][rx][bb]['all'][quan]['max'] = vy
                            if vy<outDict['antband'][antIdx][rx][bb]['all'][quan]['min']:
                                outDict['antband'][antIdx][rx][bb]['all'][quan]['min'] = vy
                            # Running mean(n+1) = mean(n) + (x(n+1)-mean(n))/(n+1)
                            meany = outDict['antband'][antIdx][rx][bb]['all'][quan]['mean']
                            runy = meany + (vy - meany)/float(ny+1)
                            outDict['antband'][antIdx][rx][bb]['all'][quan]['mean'] = runy
                            # Running var(n+1) = {n*var(n)+[x(n+1)-mean(n)][x(n+1)-mean(n+1)]}/(n+1)
                            vary = outDict['antband'][antIdx][rx][bb]['all'][quan]['var']
                            qy = ny*vary + (vy-meany)*(vy-runy)
                            outDict['antband'][antIdx][rx][bb]['all'][quan]['var'] = qy/float(ny+1)
                        outDict['antband'][antIdx][rx][bb]['all']['number'] += 1
                    if fc>=fcrange[0] and fc<fcrange[1]:
                        # this chan lies in the "inner" part of the spw
                        ncinnergood += 1
                        # Data stats
                        # By antspw per poln
                        nx = outDict['antspw'][antIdx][spwIdx][poln]['inner']['number']
                        if nx==0:
                            outDict['antspw'][antIdx][spwIdx][poln]['inner']['number'] = 1
                            for quan in quants:
                                for val in vals:
                                    outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan][val] = cdict[quan]
                        else:
                            for quan in quants:
                                vx = cdict[quan]
                                if vx>outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['max']:
                                    outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['max'] = vx
                                if vx<outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['min']:
                                    outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['min'] = vx
                                # Running mean(n+1) = mean(n) + (x(n+1)-mean(n))/(n+1)
                                meanx = outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['mean']
                                runx = meanx + (vx - meanx)/float(nx+1)
                                outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['mean'] = runx
                                # Running var(n+1) = {n*var(n)+[x(n+1)-mean(n)][x(n+1)-mean(n+1)]}/(n+1)
                                varx = outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['var']
                                qx = nx*varx + (vx-meanx)*(vx-runx)
                                outDict['antspw'][antIdx][spwIdx][poln]['inner'][quan]['var'] = qx/float(nx+1)
                                outDict['antspw'][antIdx][spwIdx][poln]['inner']['number'] += 1
                        # Now the rx-band stats
                        ny = outDict['antband'][antIdx][rx][bb]['inner']['number']
                        if ny==0:
                            outDict['antband'][antIdx][rx][bb]['inner']['number'] = 1
                            for quan in quants:
                                for val in vals:
                                    outDict['antband'][antIdx][rx][bb]['inner'][quan][val] = cdict[quan]
                        else:
                            for quan in quants:
                                vy = cdict[quan]
                                if vy>outDict['antband'][antIdx][rx][bb]['inner'][quan]['max']:
                                    outDict['antband'][antIdx][rx][bb]['inner'][quan]['max'] = vy
                                if vy<outDict['antband'][antIdx][rx][bb]['inner'][quan]['min']:
                                    outDict['antband'][antIdx][rx][bb]['inner'][quan]['min'] = vy
                                # Running mean(n+1) = mean(n) + (x(n+1)-mean(n))/(n+1)
                                meany = outDict['antband'][antIdx][rx][bb]['inner'][quan]['mean']
                                runy = meany + (vy - meany)/float(ny+1)
                                outDict['antband'][antIdx][rx][bb]['inner'][quan]['mean'] = runy
                                # Running var(n+1) = {n*var(n)+[x(n+1)-mean(n)][x(n+1)-mean(n+1)]}/(n+1)
                                vary = outDict['antband'][antIdx][rx][bb]['inner'][quan]['var']
                                qy = ny*vary + (vy-meany)*(vy-runy)
                                outDict['antband'][antIdx][rx][bb]['inner'][quan]['var'] = qy/float(ny+1)
                            outDict['antband'][antIdx][rx][bb]['inner']['number'] += 1
            npflagged = float(ncflagged)/float(nc)
            nflagged += float(ncflagged)/float(nc)
            ngood += ncgood
            ninnergood += ncinnergood
    # Assemble rest of dictionary
    outDict['antDict'] = antDict
    outDict['spwDict'] = spwDict
    outDict['rxBasebandDict'] = rxBasebandDict
    # Print summary
    print 'Within all channels:'
    print 'Found '+str(ntotal)+' total solutions with '+str(ngood)+' good (unflagged)'
    print 'Within inner '+str(fcrange[1]-fcrange[0])+' of channels:'
    print 'Found '+str(ninner)+' total solutions with '+str(ninnergood)+' good (unflagged)'
    print ''
    print '        AntID      AntName      Rx-band      Baseband    min/max(all)  min/max(inner) ALERT?'
    # Print more?
    if doprintall:
        for ant in outDict['antband'].keys():
            antName = antDict[ant]
            for rx in outDict['antband'][ant].keys():
                for bb in outDict['antband'][ant][rx].keys():
                    xmin = outDict['antband'][ant][rx][bb]['all']['amp']['min']
                    xmax = outDict['antband'][ant][rx][bb]['all']['amp']['max']
                    ymin = outDict['antband'][ant][rx][bb]['inner']['amp']['min']
                    ymax = outDict['antband'][ant][rx][bb]['inner']['amp']['max']
                    if xmax!=0.0:
                        xrat = xmin/xmax
                    else:
                        xrat = -1
                    if ymax!=0.0:
                        yrat = ymin/ymax
                    else:
                        yrat = -1
                    if yrat<0.05:
                        print ' %12s %12s %12s %12s  %12.4f  %12.4f *** ' % (ant,antName,rx,bb,xrat,yrat)
                    elif yrat<0.1:
                        print ' %12s %12s %12s %12s  %12.4f  %12.4f ** ' % (ant,antName,rx,bb,xrat,yrat)
                    elif yrat<0.2:
                        print ' %12s %12s %12s %12s  %12.4f  %12.4f * ' % (ant,antName,rx,bb,xrat,yrat)
                    else:
                        print ' %12s %12s %12s %12s  %12.4f  %12.4f ' % (ant,antName,rx,bb,xrat,yrat)
    return outDict
